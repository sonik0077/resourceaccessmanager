
INSERT INTO public.users (id, birth_date, creation_time, email, last_name, name, password) VALUES (1, '2016-07-14 19:53:53.221000', '2016-07-14 20:05:24.127000', 'admin', 'lesneu', 'pavel', 'admin');
INSERT INTO public.users (id, birth_date, creation_time, email, last_name, name, password) VALUES (2, '2000-07-14 20:07:16.096000', '2016-07-14 20:07:25.440000', 'manager', 'mike', 'jordan', 'manager');
INSERT INTO public.users (id, birth_date, creation_time, email, last_name, name, password) VALUES (3, '2010-07-14 20:08:43.406000', '2016-07-14 20:08:51.099000', 'storage_owner', 'Jim', 'book', 'storager');

INSERT INTO public.role (id, name, display_name) VALUES (1, 'ADMIN', 'Administrator');
INSERT INTO public.role (id, name, display_name) VALUES (2, 'MANAGER', 'Manager');
INSERT INTO public.role (id, name, display_name) VALUES (3, 'STORAGE_OWNER', 'Storage owner');

INSERT INTO public.privilege (id, name, display_name) VALUES (1, 'VIEW_ASSET', 'See assets');
INSERT INTO public.privilege (id, name, display_name) VALUES (2, 'EDIT_ASSET', 'Edit assets');
INSERT INTO public.privilege (id, name, display_name) VALUES (3, 'VIEW_USER', 'See users');
INSERT INTO public.privilege (id, name, display_name) VALUES (4, 'EDIT_USER', 'Edit user');
INSERT INTO public.privilege (id, name, display_name) VALUES (5, 'VIEW_ROLE', 'See role');
INSERT INTO public.privilege (id, name, display_name) VALUES (6, 'EDIT_ROLE', 'Edit roles');
INSERT INTO public.privilege (id, name, display_name) VALUES (7, 'VIEW_PRIVILEGE', 'See privileges');
INSERT INTO public.privilege (id, name, display_name) VALUES (8, 'EDIT_PRIVILEGE', 'Edit privileges');

INSERT INTO public.user_role (role_id, user_id) VALUES (1, 1);
INSERT INTO public.user_role (role_id, user_id) VALUES (2, 2);
INSERT INTO public.user_role (role_id, user_id) VALUES (3, 3);

INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 1);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 2);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 3);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 4);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 5);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 6);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 7);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (1, 8);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (2, 1);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (2, 2);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (2, 3);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (2, 4);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (3, 1);
INSERT INTO public.role_privilege (role_id, right_id) VALUES (3, 2);