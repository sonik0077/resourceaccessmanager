/**
 * Created by Lesneu Pavel on 18.07.2016.
 */
(function () {
    'use strict';

    angular.module('app').controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$location', 'LoginService'];

    function LoginController($scope, $location, LoginService) {
        
        $scope.init = function () {
            if (LoginService.getCredentials()) {
                $location.path('/');
            }
        };

        $scope.login = function () {
            $scope.dataLoading = true;
            LoginService.login($scope.username, $scope.password).then(function (data) {

                LoginService.setCredentials($scope.username, $scope.password);
                $location.path('/');

            }, function (reason) {
                if (reason.status === 401) {
                    $scope.error = 'Wrong username or password.';
                } else {
                    $scope.error = reason;
                }

                $scope.dataLoading = false;
            });
        };
        
    }

})();