/**
 * Created by Lesneu Pavel on 18.07.2016.
 */
(function () {
    'use strict';

    angular.module('app').service('LoginService', LoginService);

    LoginService.$inject = ['$http', '$cookieStore', '$rootScope'];

    function LoginService($http, $cookieStore, $rootScope) {
        var service = {};

        service.login = function (username, password) {
            return $http({
                method: 'POST',
                url: '/auth',
                headers: {
                    'Authorization': 'Basic ' + btoa(username + ':' + password)
                }
            });
        };

        service.logout = function () {
            $http.defaults.headers.common['Authorization'] = '';
            $rootScope.globals.currentUser = null;
            $cookieStore.put('globals', {});
        };

        service.setCredentials = function (username, password) {
            var authdata = btoa(username + ':' + password);

            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $cookieStore.put('globals', $rootScope.globals);
        };

        service.getCredentials = function () {
            return $rootScope.globals.currentUser;
        };

        return service;
    }

})();