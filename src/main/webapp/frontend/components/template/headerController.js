/**
 * Created by Lesneu Pavel on 19.07.2016.
 */
(function () {
    'use strict';

    angular.module('app').controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$scope', '$location', 'LoginService'];

    function HeaderController($scope, $location, LoginService) {

        $scope.location = $location;

        $scope.logout = function () {
            LoginService.logout();
            $location.path('/login');
        }

    }

})();