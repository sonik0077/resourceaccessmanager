/**
 * Created by Lesneu Pavel on 19.07.2016.
 */
(function () {
    'use strict';

    angular.module('app').service('UserService', UserService);

    UserService.$inject = ['$resource'];

    function UserService($resource) {

        var userResource = $resource('/user/:id');
        var userListResource = $resource('/user');

        var service = {
            users: null
        };

        service.getUsers = function () {
            var promise = userListResource.query().$promise;
            promise.then(function (data) {
                service.users = data;
            });
            return promise;
        };

        return service;
    }
})();