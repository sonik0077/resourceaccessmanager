/**
 * Created by Lesneu Pavel on 19.07.2016.
 */
(function () {
    'use strict';

    angular.module('app').directive('ramUsersList', UsersListDirective);
    angular.module('app').controller('UsersListController', UsersListController);

    UsersListController.$inject = ['$scope', 'UserService'];

    function UsersListDirective() {
        return {
            restrict: 'A',
            scope: {},
            controller: 'UsersListController',
            templateUrl: 'frontend/components/users/userList.html'
        }
    }

    function UsersListController($scope, UserService) {
        
        $scope.model = {
            users: UserService.users
        };
        
        $scope.init = function () {
            if (!$scope.model.users) {
                UserService.getUsers().then(function () {
                    $scope.model.users = UserService.users;
                });
            }
        };
    }

})();