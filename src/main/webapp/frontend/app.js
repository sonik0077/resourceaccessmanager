/**
 * Created by lesne on 18.07.2016.
 */
(function () {
    'use strict';

    angular.module('app', ['ngRoute', 'ngAnimate', 'ngCookies', 'ngResource']);

    angular.module('app').config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/login', {
                templateUrl: 'frontend/components/login/login.html',
                controller: 'LoginController'
            }).when('/home', {
                templateUrl: 'frontend/components/home/home.html'
            }).otherwise({
                redirectTo: '/home'
            });
        }])
        .run(['$rootScope', '$location', '$cookieStore', '$http',
            function ($rootScope, $location, $cookieStore, $http) {
                // keep user logged in after page refresh
                $rootScope.globals = $cookieStore.get('globals') || {};
                if ($rootScope.globals.currentUser) {
                    $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
                }

                $rootScope.$on('$locationChangeStart', function () {
                    // redirect to login page if not logged in
                    if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                        $location.path('/login');
                    }
                });
            }]);
})();