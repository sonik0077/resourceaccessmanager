(function () {

    function loadAll(filesArray) {
        var promises = filesArray.map(function (item) {
            return System.import(item);
        });
        return Promise.all(promises);
    }

    function getAppConfig() {
        return {
            baseURL: '/',
            map: {
                'jquery': 'frontend/vendor/jquery-2.1.1/jquery.min.js',
                'angular': 'frontend/vendor/angularjs/angular.min.js',
                'angular-route': 'frontend/vendor/angularjs/angular-route.min.js',
                'bootstrap': 'frontend/vendor/bootstrap-3.1.1/css/bootstrap.min.css',
                'app': 'frontend/app.js',
                'bootstrap.js': 'frontend/vendor/bootstrap-3.1.1/js/bootstrap.min.js',
                'definitions': 'frontend/commons/definitions/definitions.js',
                'ngAnimate': 'frontend/vendor/angularjs/angular-animate.min.js',
                'ngCookies': 'frontend/vendor/angularjs/angular-cookies.min.js',
                'ngResource': 'frontend/vendor/angularjs/angular-resource.min.js'
            },
            meta: {
                'angular': {
                    format: 'global',
                    deps: ['jquery']
                },
                'angular-route': {
                    deps: ['angular']
                },
                'app': {
                    deps: ['angular', 'angular-route', 'bootstrap.js', 'definitions', 'ngAnimate', 'ngCookies', 'ngResource']
                }
            }
        };
    }

    var commons = [
        'frontend/commons/definitions/definitions.js'
    ];

    var components = [
        'frontend/components/login/loginService.js',
        'frontend/components/login/loginController.js',
        'frontend/components/template/headerController.js',
        'frontend/components/users/usersService.js',
        'frontend/components/users/userListDirective.js'
    ];

    System.config(getAppConfig());
    var promise = System.import('app');
    promise = promise.then(function () {
        return loadAll(commons);
    });
    promise = promise.then(function () {
        return loadAll(components);
    });
    promise.then(function () {
        System.import('frontend/appRunner.js');
    });

})();