/**
 * Created by Lesneu Pavel on 18.07.2016.
 */
(function () {
    'use strict';

    angular.element(document).ready(function () {
        angular.bootstrap(document, ["app"]);
    });
})();