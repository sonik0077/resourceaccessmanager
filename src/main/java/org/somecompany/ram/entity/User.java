package org.somecompany.ram.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USERS")
public class User implements RamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL", length = 64, nullable = false)
    private String email;

    @Column(name = "NAME", length = 64, nullable = false)
    private String name;

    @Column(name = "LAST_NAME", length = 64, nullable = false)
    private String lastName;

    @Column(name = "PASSWORD", length = 255, nullable = false)
    private String password;

    @Column(name = "BIRTH_DATE")
    private Date birthDate;

    @Column(name = "CREATION_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "USER_ROLE", joinColumns = @JoinColumn(name = "USER_ID"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Role role;

}
