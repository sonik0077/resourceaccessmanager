package org.somecompany.ram.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */

@Data
@Entity
@Table(name = "ROLE")
public class Role implements RamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", length = 255, nullable = false, unique = true)
    private String name;

    @Column(name = "DISPLAY_NAME", length = 255, nullable = false, unique = true)
    private String displayName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ROLE_PRIVILEGE", joinColumns = @JoinColumn(name = "ROLE_ID"), inverseJoinColumns = @JoinColumn(name = "RIGHT_ID"))
    private Collection<Privilege> privileges;
}