package org.somecompany.ram.entity.maper;

import lombok.NonNull;
import org.somecompany.ram.dto.role.RoleDto;
import org.somecompany.ram.entity.Role;
import org.springframework.util.CollectionUtils;

import java.util.stream.Collectors;

/**
 * Created by PLPALES on 16.07.16.
 */
public class RoleMaper {
    public static Role convertFromDto(@NonNull RoleDto roleDto) {
        Role role = new Role();

        roleDto.setId(roleDto.getId());
        roleDto.setName(roleDto.getName());
        if (!CollectionUtils.isEmpty(roleDto.getPrivileges())) {
            role.setPrivileges(roleDto.getPrivileges().stream().map(PrivilegeMaper::convertFromDto).collect(Collectors.toList()));
        }
        return role;
    }
}
