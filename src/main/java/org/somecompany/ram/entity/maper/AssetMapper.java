package org.somecompany.ram.entity.maper;

import lombok.NonNull;
import org.somecompany.ram.dto.asset.AssetDto;
import org.somecompany.ram.entity.Asset;
import org.somecompany.ram.util.DateUtil;

import java.text.ParseException;

/**
 * Created by Pavel Lesneu on 13.07.2016.
 */
public class AssetMapper {

    public static Asset convertFromDto(@NonNull AssetDto assetDto) {
        Asset asset = new Asset();
        asset.setId(assetDto.getId());
        asset.setName(assetDto.getName());
        try {
            if (assetDto.getCreationDate() != null) {
                asset.setCreationTime(DateUtil.DEFAULT_DATE_FORMAT.parse(assetDto.getCreationDate()));
            }
        } catch (ParseException e) {
            asset.setCreationTime(null);
        }
        return asset;
    }
}
