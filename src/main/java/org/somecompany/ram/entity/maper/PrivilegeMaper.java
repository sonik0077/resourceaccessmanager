package org.somecompany.ram.entity.maper;

import lombok.NonNull;
import org.somecompany.ram.dto.privilege.PrivilegeDto;
import org.somecompany.ram.entity.Privilege;

/**
 * Created by PLPALES on 16.07.16.
 */
public class PrivilegeMaper {
    public static Privilege convertFromDto(@NonNull PrivilegeDto privilegeDto) {
        Privilege privilege = new Privilege();
        privilege.setId(privilegeDto.getId());
        privilege.setName(privilegeDto.getName());
        return privilege;
    }
}
