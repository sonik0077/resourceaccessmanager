package org.somecompany.ram.entity.maper;

import lombok.NonNull;
import org.somecompany.ram.dto.user.UserDetailsDto;
import org.somecompany.ram.entity.Role;
import org.somecompany.ram.entity.User;
import org.somecompany.ram.util.DateUtil;
import org.springframework.util.StringUtils;

import java.text.ParseException;

/**
 * Created by PLPALES on 16.07.16.
 */
public class UserMaper {
    public static User convertFromDto(@NonNull UserDetailsDto userDetailsDto) {
        User user = new User(userDetailsDto.getId(), userDetailsDto.getEmail(), userDetailsDto.getName(),
                userDetailsDto.getLastName() , null, null, null, null);

        if (!StringUtils.isEmpty(userDetailsDto.getBirthDate())) {
            try {
                user.setBirthDate(DateUtil.DEFAULT_DATE_FORMAT.parse(userDetailsDto.getBirthDate()));
            } catch (ParseException e) {
                user.setBirthDate(null);
            }
        }

        if (userDetailsDto.getRole() != null) {
            Role role = null;
            user.setRole(role);
        }
        return  user;
    }
}
