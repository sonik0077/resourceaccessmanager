package org.somecompany.ram.util;

import java.text.SimpleDateFormat;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */
public class DateUtil {

    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd");
    public static final SimpleDateFormat DEFAULT_TIME_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
}
