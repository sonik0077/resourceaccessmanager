package org.somecompany.ram.builder;

import lombok.AllArgsConstructor;
import org.somecompany.ram.dto.asset.AssetDto;
import org.somecompany.ram.dto.maper.AssetDtoMapper;
import org.somecompany.ram.entity.Asset;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */
@AllArgsConstructor
public class AssetListBuilder {

    Iterable<Asset> assets;

    public List<AssetDto> build() {
        if (assets == null) {
            return null;
        }
        List<AssetDto> assetList = new ArrayList<>();
        assets.forEach(asset -> assetList.add(AssetDtoMapper.convertFromEntity(asset)));
        return assetList;
    }
}
