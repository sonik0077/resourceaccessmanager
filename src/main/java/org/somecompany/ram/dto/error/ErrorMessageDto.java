package org.somecompany.ram.dto.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.somecompany.ram.dto.Dto;

/**
 * Created by Pavel Lesneu on 13.07.2016.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessageDto implements Dto {
    private String code;
    private String message;
    private String details;
}
