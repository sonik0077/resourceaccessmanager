package org.somecompany.ram.dto.privilege;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.somecompany.ram.dto.Dto;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrivilegeDto implements Dto{
    private Long id;
    private String name;
    private String displayName;
}
