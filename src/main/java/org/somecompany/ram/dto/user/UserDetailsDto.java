package org.somecompany.ram.dto.user;

import lombok.*;
import org.somecompany.ram.dto.role.RoleDto;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */

@NoArgsConstructor
public class UserDetailsDto extends UserDto {
    @Getter
    @Setter
    private RoleDto role;

    public UserDetailsDto(@NonNull UserDto userDto) {
        setId(userDto.getId());
        setName(userDto.getName());
        setLastName(userDto.getLastName());
        setEmail(userDto.getEmail());
        setBirthDate(userDto.getBirthDate());
    }

}
