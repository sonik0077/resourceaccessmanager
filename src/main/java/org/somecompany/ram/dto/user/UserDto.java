package org.somecompany.ram.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.somecompany.ram.dto.Dto;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Dto {

    private Long id;
    private String email;
    private String name;
    private String lastName;
    private String birthDate;

}
