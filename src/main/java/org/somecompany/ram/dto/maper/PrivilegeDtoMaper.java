package org.somecompany.ram.dto.maper;

import org.somecompany.ram.dto.privilege.PrivilegeDto;
import org.somecompany.ram.entity.Privilege;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
public class PrivilegeDtoMaper {
    public static PrivilegeDto convertFromEntity(Privilege privilege) {
        return new PrivilegeDto(privilege.getId(), privilege.getName(), privilege.getDisplayName());
    }
}
