package org.somecompany.ram.dto.maper;

import lombok.NonNull;
import org.somecompany.ram.dto.user.UserDetailsDto;
import org.somecompany.ram.dto.user.UserDto;
import org.somecompany.ram.entity.User;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
public class UserDtoMaper {
    public static UserDto convertFromEntity(@NonNull User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        return userDto;
    }

    public static UserDetailsDto convertFromEntityToDetails(@NonNull User user) {
        UserDetailsDto userDetailsDto = new UserDetailsDto(convertFromEntity(user));
        userDetailsDto.setRole(RoleDtoMaper.convertFromEntity(user.getRole()));
        return userDetailsDto;
    }
}
