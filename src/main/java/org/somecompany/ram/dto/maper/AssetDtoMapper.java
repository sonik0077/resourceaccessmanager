package org.somecompany.ram.dto.maper;

import lombok.NonNull;
import org.somecompany.ram.dto.asset.AssetDto;
import org.somecompany.ram.entity.Asset;
import org.somecompany.ram.util.DateUtil;

/**
 * Created by Pavel Lesneu on 13.07.2016.
 */
public class AssetDtoMapper {

    public static AssetDto convertFromEntity(@NonNull Asset asset) {
        AssetDto assetDto = new AssetDto();
        assetDto.setId(asset.getId());
        assetDto.setName(asset.getName());
        if (asset.getCreationTime() != null) {
            assetDto.setCreationDate(DateUtil.DEFAULT_DATE_FORMAT.format(asset.getCreationTime()));
        }
        return assetDto;
    }
}
