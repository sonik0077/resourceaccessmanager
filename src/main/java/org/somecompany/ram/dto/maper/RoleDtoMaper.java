package org.somecompany.ram.dto.maper;

import org.somecompany.ram.dto.privilege.PrivilegeDto;
import org.somecompany.ram.dto.role.RoleDto;
import org.somecompany.ram.entity.Role;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
public class RoleDtoMaper {
    public static RoleDto convertFromEntity(Role role) {
        RoleDto roleDto = new RoleDto(role.getId(), role.getName(), role.getDisplayName(), null);
        List<PrivilegeDto> privileges = role.getPrivileges().stream()
                .map(PrivilegeDtoMaper::convertFromEntity)
                .collect(Collectors.toList());
        roleDto.setPrivileges(privileges);
        return roleDto;
    }
}
