package org.somecompany.ram.dto.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.somecompany.ram.dto.Dto;
import org.somecompany.ram.dto.privilege.PrivilegeDto;

import java.util.List;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto implements Dto{

    private Long id;
    private String name;
    private String displayName;

    private List<PrivilegeDto> privileges;
}
