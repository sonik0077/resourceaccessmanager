package org.somecompany.ram.dto.asset;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.somecompany.ram.dto.Dto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssetDto implements Dto {
    private Long id;
    private String name;
    private String creationDate;
}
