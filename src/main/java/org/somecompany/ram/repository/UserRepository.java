package org.somecompany.ram.repository;

import org.somecompany.ram.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */

public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);
}
