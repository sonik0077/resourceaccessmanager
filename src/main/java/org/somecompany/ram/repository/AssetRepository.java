package org.somecompany.ram.repository;

import org.somecompany.ram.entity.Asset;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Pavel Lesneu on 11.07.2016.
 */
public interface AssetRepository extends CrudRepository<Asset, Long>{

    Asset findByName(String name);

}
