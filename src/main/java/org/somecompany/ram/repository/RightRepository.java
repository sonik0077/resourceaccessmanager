package org.somecompany.ram.repository;

import org.somecompany.ram.entity.Privilege;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */
public interface RightRepository extends CrudRepository<Privilege, Long> {
}
