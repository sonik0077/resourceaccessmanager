package org.somecompany.ram.repository;

import org.somecompany.ram.entity.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByName(String name);
}
