package org.somecompany.ram.web;

import org.somecompany.ram.dto.privilege.PrivilegeDto;
import org.somecompany.ram.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Pavel Lesneu on 17.07.2016.
 */
@RestController
@RequestMapping("privilege")
public class PrivilegeController {

    @Autowired
    private PrivilegeService privilegeService;

    @PreAuthorize("hasAuthority('VIEW_PRIVILEGE')")
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<PrivilegeDto> getRoles() {
        return privilegeService.getPrivileges();
    }

}
