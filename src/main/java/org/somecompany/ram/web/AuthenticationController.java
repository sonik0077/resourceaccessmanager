package org.somecompany.ram.web;

import org.somecompany.ram.dto.user.UserDetailsDto;
import org.somecompany.ram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by Pavel Lesneu on 19.07.2016.
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(name = "/", method = RequestMethod.POST)
    public UserDetailsDto authenticate(Principal principal) {
        return userService.getUserDetailsByEmail(principal.getName());
    }
}
