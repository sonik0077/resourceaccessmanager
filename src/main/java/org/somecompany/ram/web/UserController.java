package org.somecompany.ram.web;

import org.somecompany.ram.dto.user.UserDetailsDto;
import org.somecompany.ram.dto.user.UserDto;
import org.somecompany.ram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Pavel Lesneu on 17.07.2016.
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('VIEW_USER')")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public UserDetailsDto getUser(@PathVariable("id") Long id) {
        return userService.getUserDetails(id);
    }

    @PreAuthorize("hasAuthority('VIEW_USER')")
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<UserDto> getUsers() {
        return userService.getUsers();
    }

    @PreAuthorize("hasAuthority('EDIT_USER')")
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void addUser(@RequestBody UserDetailsDto user) {
        userService.createUser(user);
    }

    @PreAuthorize("hasAuthority('EDIT_USER')")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void saveUser(@PathVariable Long id, @RequestBody UserDetailsDto user) {
        userService.saveUser(id, user);
    }

    @PreAuthorize("hasAuthority('VIEW_USER')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void removeUser(@PathVariable("id") Long id) {
        userService.removeUser(id);
    }

    @PreAuthorize("hasAuthorities('EDIT_USER')")
    @RequestMapping(value = "{id}/role/{roleId}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void changeUserRole(@PathVariable Long id, @PathVariable Long roleId) {
        userService.changeUserRole(id, roleId);
    }
}
