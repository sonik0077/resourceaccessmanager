package org.somecompany.ram.web;

import org.somecompany.ram.dto.asset.AssetDto;
import org.somecompany.ram.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("asset")
public class AssetController {

    @Autowired
    private AssetService assetService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public AssetDto getAsset(@PathVariable("id") Long id) {
        return assetService.getAsset(id);
    }

    @PreAuthorize("hasAuthority('VIEW_ASSET')")
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<AssetDto> getAssets() {
        return assetService.getAssets();
    }

    @PreAuthorize("hasAuthority('EDIT_ASSET')")
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void addAsset(@RequestBody AssetDto asset) {
        assetService.addAsset(asset);
    }

    @PreAuthorize("hasAuthority('EDIT_ASSET')")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void saveAsset(@PathVariable Long id, @RequestBody AssetDto asset) {
        assetService.saveAsset(id, asset);
    }

    @PreAuthorize("hasAuthority('VIEW_ASSET')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void removeAsset(@PathVariable("id") Long id) {
        assetService.removeAsset(id);
    }
}