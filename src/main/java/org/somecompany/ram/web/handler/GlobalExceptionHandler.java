package org.somecompany.ram.web.handler;

import org.somecompany.ram.dto.error.ErrorMessageDto;
import org.somecompany.ram.exception.RamRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by Pavel Lesneu on 13.07.2016.
 */

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessageDto handleUnexpectedExceptions(Exception e) {
        e.printStackTrace();
        return new ErrorMessageDto("UNEXPECTED", "Unexpected server error", e.getMessage());
    }

    @ExceptionHandler({RamRuntimeException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessageDto handleRamExceptions(RamRuntimeException e) {
        e.printStackTrace();
        return new ErrorMessageDto("VIOLATION", e.getMessage(), e.getDetails());
    }

    @ExceptionHandler({AccessDeniedException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorMessageDto handleRamExceptions(AccessDeniedException e) {
        e.printStackTrace();
        return new ErrorMessageDto("ACCESS_DENIED", e.getMessage(),"You don't have permissions to execute this action");
    }
}
