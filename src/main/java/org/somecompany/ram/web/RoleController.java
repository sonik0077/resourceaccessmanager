package org.somecompany.ram.web;

import org.somecompany.ram.dto.role.RoleDto;
import org.somecompany.ram.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Pavel Lesneu on 17.07.2016.
 */
@RestController
@RequestMapping("role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PreAuthorize("hasAuthority('VIEW_ROLE')")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public RoleDto getRole(@PathVariable("id") Long id) {
        return roleService.getRole(id);
    }

    @PreAuthorize("hasAuthority('VIEW_ROLE')")
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<RoleDto> getRoles() {
        return roleService.getRoles();
    }

    @PreAuthorize("hasAuthority('EDIT_ROLE')")
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void addRole(@RequestBody RoleDto role) {
        roleService.createRole(role);
    }

    @PreAuthorize("hasAuthority('EDIT_ROLE')")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void saveRole(@PathVariable Long id, @RequestBody RoleDto role) {
        roleService.saveRole(id, role);
    }

    @PreAuthorize("hasAuthority('EDIT_ROLE')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void removeRole(@PathVariable("id") Long id) {
        roleService.removeRole(id);
    }
}
