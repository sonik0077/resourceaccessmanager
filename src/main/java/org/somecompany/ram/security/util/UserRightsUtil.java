package org.somecompany.ram.security.util;

import lombok.NonNull;

import java.util.StringJoiner;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
public class UserRightsUtil {

    public static String hasAuthority(@NonNull String authority) {
        return "hasAuthority('" + authority + "')";
    }

    public static String hasAuthorities(@NonNull String... authorities) {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (String authority : authorities) {
            stringJoiner.add(authority);
        }
        return "hasAuthorities('" + stringJoiner.toString() + "')";
    }
}
