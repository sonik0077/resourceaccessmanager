package org.somecompany.ram.security;

import org.somecompany.ram.entity.Privilege;
import org.somecompany.ram.entity.User;
import org.somecompany.ram.repository.UserRepository;
import org.somecompany.ram.security.model.maper.UserDetailsMaper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created by Pavel Lesneu on 14.07.2016.
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username);
        Collection<Privilege> privileges = user.getRole().getPrivileges();
        return UserDetailsMaper.convertFromEntities(user, privileges);
    }
}
