package org.somecompany.ram.security.model.maper;

import lombok.NonNull;
import org.somecompany.ram.entity.Privilege;
import org.somecompany.ram.entity.User;
import org.somecompany.ram.security.model.UserAuthorityImpl;
import org.somecompany.ram.security.model.UserDetailsImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Pavel Lesneu on 14.07.2016.
 */
public class UserDetailsMaper {
    public static UserDetails convertFromEntities(@NonNull User user, @NonNull Collection<Privilege> privileges) {
        UserDetailsImpl userDetails = new UserDetailsImpl();
        userDetails.setUsername(user.getEmail());
        userDetails.setPassword(user.getPassword());
        List<GrantedAuthority> grantedAuthorities = privileges.stream()
                .map(privilege -> new UserAuthorityImpl(privilege.getName())).collect(Collectors.toList());
        userDetails.setAuthorities(grantedAuthorities);
        return userDetails;
    }
}
