package org.somecompany.ram.security.model;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
public class UserRights {
    public static final String VIEW_ASSET = "VIEW_ASSET";
    public static final String EDIT_ASSET = "EDIT_ASSET";
    public static final String VIEW_ROLE = "VIEW_ROLE";
}
