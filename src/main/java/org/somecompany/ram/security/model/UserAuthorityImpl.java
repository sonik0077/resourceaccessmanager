package org.somecompany.ram.security.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

/**
 * Created by Pavel Lesneu on 14.07.2016.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthorityImpl implements GrantedAuthority {
    private String authority;
}
