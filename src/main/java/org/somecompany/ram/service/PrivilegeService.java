package org.somecompany.ram.service;

import org.somecompany.ram.dto.privilege.PrivilegeDto;

import java.util.List;

/**
 * Created by Pavel Lesneu on 17.07.2016.
 */
public interface PrivilegeService {
    List<PrivilegeDto> getPrivileges();
}
