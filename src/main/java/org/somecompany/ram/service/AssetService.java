package org.somecompany.ram.service;

import org.somecompany.ram.dto.asset.AssetDto;

import java.util.List;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */

public interface AssetService {

    List<AssetDto> getAssets();

    AssetDto getAsset(Long id);

    void addAsset(AssetDto assetDto);

    void removeAsset(Long id);

    void saveAsset(Long id, AssetDto asset);
}
