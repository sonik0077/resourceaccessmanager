package org.somecompany.ram.service;

import org.somecompany.ram.dto.user.UserDetailsDto;
import org.somecompany.ram.dto.user.UserDto;

import java.util.List;

/**
 * Created by Pavel Lesneu on 13.07.2016.
 */
public interface UserService {
    List<UserDto> getUsers();

    UserDetailsDto getUserDetails(Long id);

    UserDetailsDto getUserDetailsByEmail(String email);

    void createUser(UserDetailsDto userDetailsDto);

    void removeUser(Long id);

    void saveUser(Long id, UserDetailsDto userDetailsDto);

    void changeUserRole(Long id, Long roleId);
}
