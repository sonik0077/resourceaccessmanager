package org.somecompany.ram.service;

import org.somecompany.ram.dto.role.RoleDto;

import java.util.List;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
public interface RoleService {
    List<RoleDto> getRoles();
    RoleDto getRole(Long id);
    void removeRole(Long id);
    void createRole(RoleDto roleDto);
    void saveRole(Long id, RoleDto roleDto);
}
