package org.somecompany.ram.service.impl;

import org.somecompany.ram.dto.maper.PrivilegeDtoMaper;
import org.somecompany.ram.dto.privilege.PrivilegeDto;
import org.somecompany.ram.entity.Privilege;
import org.somecompany.ram.repository.RightRepository;
import org.somecompany.ram.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Lesneu on 17.07.2016.
 */
@Component
public class PrivilegesServiceImpl implements PrivilegeService {

    @Autowired
    private RightRepository rightRepository;

    @Override
    public List<PrivilegeDto> getPrivileges() {
        List<PrivilegeDto> privilegeDtoList = new ArrayList<>();
        Iterable<Privilege> privileges = rightRepository.findAll();
        for (Privilege privilege : privileges) {
            privilegeDtoList.add(PrivilegeDtoMaper.convertFromEntity(privilege));
        }
        return privilegeDtoList;
    }
}
