package org.somecompany.ram.service.impl;

import lombok.NonNull;
import org.somecompany.ram.builder.AssetListBuilder;
import org.somecompany.ram.dto.asset.AssetDto;
import org.somecompany.ram.dto.maper.AssetDtoMapper;
import org.somecompany.ram.entity.Asset;
import org.somecompany.ram.entity.maper.AssetMapper;
import org.somecompany.ram.exception.RamRuntimeException;
import org.somecompany.ram.repository.AssetRepository;
import org.somecompany.ram.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by Pavel Lesneu on 12.07.2016.
 */
@Component
public class AssetServiceImpl implements AssetService {

    @Autowired
    private AssetRepository assetRepository;

    public List<AssetDto> getAssets() {
        Iterable<Asset> assets = assetRepository.findAll();
        AssetListBuilder assetListBuilder = new AssetListBuilder(assets);
        return assetListBuilder.build();
    }

    public AssetDto getAsset(@NonNull final Long id) {
        Asset asset = assetRepository.findOne(id);
        return AssetDtoMapper.convertFromEntity(asset);
    }

    public void addAsset(@NonNull AssetDto assetDto) {
        validateAssetName(assetDto.getName());
        Asset asset = AssetMapper.convertFromDto(assetDto);
        asset.setId(null);
        asset.setCreationTime(new Date());
        assetRepository.save(asset);
    }

    public void removeAsset(@NonNull final Long id) {
        assetRepository.delete(id);
    }

    public void saveAsset(@NonNull Long id, @NonNull AssetDto assetDto) {
        validateAssetName(assetDto.getName());
        Asset asset = assetRepository.findOne(id);
        asset.setName(assetDto.getName());
        assetRepository.save(asset);
    }

    private void validateAssetName(String name) {
        if (name == null || "".equals(name)) {
            throw new RamRuntimeException("Asset name is empty.", "Asset name should not be empty.");
        }
        Asset asset = assetRepository.findByName(name);
        if (asset != null) {
            throw new RamRuntimeException("Asset with this name already exists.", "Try to use different name. blabla");
        }
    }
}
