package org.somecompany.ram.service.impl;

import org.somecompany.ram.dto.maper.UserDtoMaper;
import org.somecompany.ram.dto.user.UserDetailsDto;
import org.somecompany.ram.dto.user.UserDto;
import org.somecompany.ram.entity.Role;
import org.somecompany.ram.entity.User;
import org.somecompany.ram.entity.maper.UserMaper;
import org.somecompany.ram.exception.RamRuntimeException;
import org.somecompany.ram.repository.RoleRepository;
import org.somecompany.ram.repository.UserRepository;
import org.somecompany.ram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Lesneu on 15.07.2016.
 */
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<UserDto> getUsers() {
        List<UserDto> dtoList = new ArrayList<>();
        Iterable<User> users = userRepository.findAll();
        for (User user : users) {
            dtoList.add(UserDtoMaper.convertFromEntity(user));
        }
        return dtoList;
    }

    @Override
    public UserDetailsDto getUserDetails(Long id) {
        if (id == null) {
            throw new RamRuntimeException("User id cannot be empty.");
        }
        User user = userRepository.findOne(id);
        return UserDtoMaper.convertFromEntityToDetails(user);
    }

    @Override
    public UserDetailsDto getUserDetailsByEmail(String email) {
        if (email == null) {
            throw new RamRuntimeException("User email cannot be empty.");
        }
        User user = userRepository.findByEmail(email);
        return UserDtoMaper.convertFromEntityToDetails(user);
    }

    @Override
    public void createUser(UserDetailsDto userDetailsDto) {
        validateUser(userDetailsDto);
        userDetailsDto.setId(null);
        userRepository.save(UserMaper.convertFromDto(userDetailsDto));
    }

    @Override
    public void removeUser(Long id) {
        if (id == null) {
            throw new RamRuntimeException("User id cannot be empty.");
        }
        userRepository.delete(id);
    }

    @Override
    public void saveUser(Long id, UserDetailsDto userDetailsDto) {
        if (id == null) {
            throw new RamRuntimeException("User id cannot be empty.");
        }
        userDetailsDto.setId(id);
        validateUser(userDetailsDto);
        userRepository.save(UserMaper.convertFromDto(userDetailsDto));
    }

    @Override
    public void changeUserRole(Long id, Long roleId) {
        if (id == null || roleId == null) {
            throw new RamRuntimeException("User ID and Role ID are required");
        }
        Role role = roleRepository.findOne(roleId);
        if (role == null) {
            throw new RamRuntimeException("Role with ID = " + roleId + " doesn't exist.");
        }
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new RamRuntimeException("User with ID = " + roleId + " doesn't exist.");
        }
        user.setRole(role);
        userRepository.save(user);
    }

    private void validateUser(UserDetailsDto userDetailsDto) {
        if (userDetailsDto == null) {
            throw new RamRuntimeException("User info cannot be empty.");
        }
        if (StringUtils.isEmpty(userDetailsDto.getEmail())) {
            throw new RamRuntimeException("User email cannot be empty.");
        }
        if (StringUtils.isEmpty(userDetailsDto.getName()) || StringUtils.isEmpty(userDetailsDto.getLastName())) {
            throw new RamRuntimeException("User name and last name are mandatory.");
        }
        User user = userRepository.findByEmail(userDetailsDto.getEmail());
        if (user != null && !user.getId().equals(userDetailsDto.getId())) {
            throw new RamRuntimeException("User with this name already exists.");
        }
    }
}
