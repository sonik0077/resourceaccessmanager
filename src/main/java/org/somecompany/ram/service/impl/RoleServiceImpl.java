package org.somecompany.ram.service.impl;

import org.somecompany.ram.dto.maper.RoleDtoMaper;
import org.somecompany.ram.dto.role.RoleDto;
import org.somecompany.ram.entity.Role;
import org.somecompany.ram.entity.maper.RoleMaper;
import org.somecompany.ram.exception.RamRuntimeException;
import org.somecompany.ram.repository.RoleRepository;
import org.somecompany.ram.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Lesneu on 17.07.2016.
 */
@Component
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<RoleDto> getRoles() {
        List<RoleDto> dtoList = new ArrayList<>();
        Iterable<Role> roles = roleRepository.findAll();
        for (Role role : roles) {
            dtoList.add(RoleDtoMaper.convertFromEntity(role));
        }
        return dtoList;
    }

    @Override
    public RoleDto getRole(Long id) {
        if (id == null) {
            throw new RamRuntimeException("Role id cannot be empty.");
        }
        Role role = roleRepository.findOne(id);
        return RoleDtoMaper.convertFromEntity(role);
    }

    @Override
    public void removeRole(Long id) {
        if (id == null) {
            throw new RamRuntimeException("Role id cannot be empty.");
        }
        roleRepository.delete(id);
    }

    @Override
    public void createRole(RoleDto roleDto) {
        validateRole(roleDto);
        roleDto.setId(null);
        roleRepository.save(RoleMaper.convertFromDto(roleDto));
    }

    @Override
    public void saveRole(Long id, RoleDto roleDto) {
        if (id == null) {
            throw new RamRuntimeException("Role id cannot be empty.");
        }
        roleDto.setId(id);
        validateRole(roleDto);
        roleRepository.save(RoleMaper.convertFromDto(roleDto));
    }

    private void validateRole(RoleDto roleDto) {
        if (roleDto == null) {
            throw new RamRuntimeException("Role info cannot be empty.");
        }
        if (StringUtils.isEmpty(roleDto.getName())) {
            throw new RamRuntimeException("Role name cannot be empty.");
        }
        if (StringUtils.isEmpty(roleDto.getName()) || StringUtils.isEmpty(roleDto.getDisplayName())) {
            throw new RamRuntimeException("Role name and display name are mandatory.");
        }
        Role role = roleRepository.findByName(roleDto.getName());
        if (role != null && !role.getId().equals(roleDto.getId())) {
            throw new RamRuntimeException("Role with this name already exists.");
        }
    }
}
