package org.somecompany.ram.exception;

import lombok.Getter;
import lombok.NonNull;

/**
 * Created by Pavel Lesneu on 13.07.2016.
 */
public class RamRuntimeException extends RuntimeException {
    @Getter
    private String details;

    public RamRuntimeException(@NonNull String message) {
        super(message);
    }

    public RamRuntimeException(@NonNull String message, @NonNull String details) {
        super(message);
        this.details = details;
    }
}
